const fahrToCels = (fahrenheit) => {
    return (fahrenheit - 32) / 1.8;
};

module.exports = {
  fahrToCels
};